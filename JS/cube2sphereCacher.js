// cube2sphereCacher.js
// node cube2sphereCacher.js width height aaSamples

var cube2sphereCacher = function() {

  var fs = require('fs');
  var args = process.argv.splice(2);
  var HALF_PI = Math.PI * 0.5;
  var EQUALITY_THRESHOLD = 0.000001;

  function areEqual(a, b) {

    return (Math.abs(a - b) <= EQUALITY_THRESHOLD);

  };

  function getPointOnUnitCube(x, y, z) {

    var tmin = -100, tmax = 100;
    var result = {x: 1, y: 0, z: 0};

    if(!areEqual(x, 0)) {

      var xInv = 1 / x;
      var x1 = -xInv;
      var x2 = xInv;
      tmin = Math.max(tmin, Math.min(x1, x2));
      tmax = Math.min(tmax, Math.max(x1, x2));

    }

    if(!areEqual(y, 0)) {

      var yInv = 1 / y;
      var y1 = -yInv;
      var y2 = yInv;;
      tmin = Math.max(tmin, Math.min(y1, y2));
      tmax = Math.min(tmax, Math.max(y1, y2));

    }

    if(!areEqual(z, 0)) {

      var zInv = 1 / z;
      var z1 = -zInv;
      var z2 = zInv;;
      tmin = Math.max(tmin, Math.min(z1, z2));
      tmax = Math.min(tmax, Math.max(z1, z2));

    }
    result.x = tmin * x;
    result.y = tmin * y;
    result.z = tmin * z;
    return result;

  };

  function begin() {

    var width = parseInt(args[0]);
    var height = parseInt(args[1]);
    var aaSamples = parseInt(args[2]);
    var denomX = 1 / (width - 1);
    var denomY = 1 / (height - 1);
    var sampleWidth = denomX / aaSamples;
    var sampleHeight = denomY / aaSamples;
    var sampleWeight = 1 / (aaSamples * aaSamples);
    var cache = {};

    for(var i = 0; i < width; i++) {

      for(var j = 0; j < height; j++) {

        // Normalized
        var nx = 2 * i * denomX - 1;
        var ny = 2 * j * denomY - 1;

        var avgColor = {r: 0, g: 0, b: 0, a: 100};
        for(var si = 1; si <= aaSamples; si++) {

          for(var sj = 1; sj <= aaSamples; sj++) {
            
            var u = nx + si * sampleWidth * 0.5;
            var v = ny + sj * sampleHeight * 0.5;

            // Spherical coords
            var theta = u * Math.PI;
            var phi = v * HALF_PI;

            // Cartesian coords
            var x = Math.cos(theta) * Math.cos(phi);
            var y = Math.sin(phi);
            var z = Math.sin(theta) * Math.cos(phi);

            // Coordinates on cube
            var p = getPointOnUnitCube(x, y, z);

            // Cache it
            if(cache[theta] !== undefined) {

              if(cache[theta][phi] == undefined) {

                cache[theta][phi] = {x: p.x, y: p.y, z: p.z};

              }

            } else {

              cache[theta] = {};

            }

          }

        }

      }

    }

    fs.writeFile('.cube2sphereCache.json', JSON.stringify(cache), function(error) {

      if(error) {

        throw error;

      } else {

        console.log('Saved .cube2sphereCache.json!');

      } 

    });

  };

  begin();

};

cube2sphereCacher();