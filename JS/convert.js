// convert.js
// node convert.js mode width height aa, jitter, out, in[or (fblrtb)]

var convert = function() {

  var lwip = require('lwip');
  var converters = require('./converters.js');
  var args = process.argv.slice(2);
  var opts = {};
  opts.aaSamples = parseInt(args[3]);
  opts.jittered = args[4] !== 'true' ? false : true;
  function doConversion(converter, inputImages, outputImage) {

    // console.log('inputImages: ');
    // console.log(inputImages);
    // console.log('output: ');
    // console.log(outputImage);

    var output = converters[converter](inputImages, outputImage, opts);
    output.writeFile(args[5], function(error) {

      if(error) {

        process.send(error.toString());
        return;

      }

    });
  
  };

  function loadAndConvert(inputFilesNames, numFiles, converter) {

    var inputImages = {front: 'front', back: 'back', left: 'left', right: 'right', top: 'top', bottom: 'bottom'};
    var outputImage;
    var filesLoaded = 0;

    // Load the input image(s) in parallel
    for(var i in inputFilesNames) {

      function helperFunc(index) {

        lwip.open(inputFilesNames[index], function(error, image) {
          
          if(error) {

            process.send(error);

          } else {

            inputImages[index] = image;
            if(filesLoaded >= numFiles) {

              doConversion(converter, inputImages, outputImage);
              return;
            
            }
            filesLoaded++;

          }

        });

      };
      helperFunc(i);

    }
    // Create the output image
    var width = parseInt(args[1]);
    var height = parseInt(args[2]);
    lwip.create(width, height, 'white', function(error, image) {

      if(error) {

        process.send(error);

      } else {

        outputImage = image;
        if(filesLoaded >= numFiles) {

          doConversion(converter, inputImages, outputImage);
          return;
        
        }
        
        filesLoaded++;

      }

    });

  };

  function begin() {

    var inputFilesNames = {};
    var mode = parseInt(args[0]);
    // console.log('  mode is : ' + mode);
    switch(mode) {

      // cube to sphere
      case 1:
        inputFilesNames.front = args[6];
        inputFilesNames.back = args[7];
        inputFilesNames.left = args[8];
        inputFilesNames.right = args[9];;
        inputFilesNames.top = args[10];;
        inputFilesNames.bottom = args[11];;
        loadAndConvert(inputFilesNames, 6, 'cube2sphere');
        break;
      // sphere to cube
      case 2:
        inputFilesNames.input = args[6];
        loadAndConvert(inputFilesNames, 1, 'sphere2cube');
        break;
      // cube to cyl
      case 3:
        inputFilesNames.input = args[6];
        loadAndConvert(inputFilesNames, 1, 'cube2cyl');
        break;
      // cyl to cube
      case 4:
        inputFilesNames.input = args[6];
        loadAndConvert(inputFilesNames, 1, 'cyl2cube');
        break;
      // sphere to cyl
      case 5:
        inputFilesNames.input = args[6];
        loadAndConvert(inputFilesNames, 1, 'sphere2cyl');
        break;
      // cyl to sphere
      case 6:
        inputFilesNames.input = args[6];
        loadAndConvert(inputFilesNames, 1, 'cyl2sphere');
        break;

    };

  };

  begin();

};

convert();