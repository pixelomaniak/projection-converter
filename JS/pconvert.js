// pconvert.js

var pconvert = function() {

  var optimist = require('optimist');
  var os = require('os');
  var fs = require('fs');
  var sugar = require('sugar');
  var spawn = require('child_process');
  var convert = require('./convert.js');
  // var cacher = require('./cacher.js');

  var argv = optimist(process.argv.slice(2));
  var usage = 'Usage: $ -m[conversion mode] -in[source folder] -out[dest folder]';
  usage += '-w(ouput width) -h(ouput height) -s(start frame) -e(end frame) -aa(number of aa samples) ';
  usage += ' -j(jittered sampling) - frmt(output format)';
  argv = argv.demand(['m', 'in', 'out'])
    .default({w: 1280, h: 720, aa: 3, j: true, frmt: '.png'})
    .usage(usage)
    .argv;
  argv.frmt = argv.frmt !== 'jpg' ? '.png' : '.jpg';
  var IMAGES_PER_CUBE_MAP = 6;
  var files, numFrames, numProcesses, currentFrameIndex, numActiveProcesses = 0;
  var currentInterval;

  function spawnProcesses() {

    console.log('started a new process');
    var params = [argv.m, argv.w, argv.h, argv.aa, argv.j];
    if(argv.m == 1) {

      var index = currentFrameIndex * IMAGES_PER_CUBE_MAP;
      var out = argv.out + '/frame_' + currentFrameIndex + argv.frmt;
      var front = argv.in + files[index];
      var back = argv.in + files[index + 1];
      var left = argv.in + files[index + 2];
      var right = argv.in + files[index + 3];
      var top = argv.in + files[index + 4];
      var bottom = argv.in + files[index + 5];
      params.push(out);
      params.push(front);
      params.push(back);
      params.push(left);
      params.push(right);
      params.push(top);
      params.push(bottom);

    } else {

      var out = argv.out + '/frame_' + currentFrameIndex + argv.frmt;
      var input = files[currentFrameIndex];
      params.push(out);
      params.push(input);

    }
    // console.log(currentFrameIndex);
    // console.log(params);
    numActiveProcesses++;
    var process = spawn.fork('./convert', params);
    process.on('close', function(code) {

      if(code === 0) {

        currentFrameIndex++;
        numActiveProcesses--;
        doConversion();

      } else {

        console.log('+'.repeat(16) + '\nError! process ' + process.pid + ' has finished with code: ' + code + '\n' +
          '*'.repeat(16));
        // clearInterval(currentInterval);
        return;

      }

    });

    process.on('message', function(message) {

      console.log('+'.repeat(16) + '\nProcess: ' + process.pid + ' says: ' + message + '\n' + '*'.repeat(16)); 

    });

  };

  function doConversion() {

    if(currentFrameIndex >= numFrames) {

      // clearInterval(currentInterval);
      console.log('Finished! ' + numFrames + ' frames converted.');
      return;

    }
    if(numActiveProcesses < numProcesses) {

      for(var i = 0; i < (numProcesses - numActiveProcesses); i++) {

        spawnProcesses();

      }

    } else {

      setTimeout(doConversion, 1000);

    }

  };

  function initConverter() {

    var numCPUs = os.cpus().length;
    files = fs.readdirSync(argv.in.toString());
    var temp = [];
    for(var i in files) {

      if(files[i].search(/\.jpg|JPG|png|PNG$/) !== -1) {

        temp.push(files[i]);
      
      }
    
    }
    files = temp;
    Array.AlphanumericSortNatural = true;
    files = files.sortBy();
    numFrames = argv.m === 1 ? Math.floor(files.length / IMAGES_PER_CUBE_MAP) : files.length;
    numProcesses = Math.min(numFrames, numCPUs);
    currentFrameIndex = 0;
    framesConverted = 0;
    if(numFrames > 0 && !isNaN(argv.s) && !isNaN(argv.e) && argv.s < numFrames && argv.e < numFrames) {

      numFrames = argv.e - argv.s + 1;
      numProcesses = Math.min(numProcesses, numFrames);
      currentFrameIndex = argv.s;

    }
    console.log('numProcesses: ' + numProcesses);

  };

  function begin() {

    initConverter();
    doConversion();
    // currentInterval = setInterval(doConversion, 10);

  };

  begin();

};

pconvert();