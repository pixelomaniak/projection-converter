// converters.js
// node converter.js mode width height aa jitter out in[or (fblrtp)]

var converters = function() {

  var fs = require('fs');
  var HALF_PI = Math.PI * 0.5;
  var EQUALITY_THRESHOLD = 0.000001;

  function areEqual(a, b) {

    return (Math.abs(a - b) <= EQUALITY_THRESHOLD);

  };
  
  return {

    cube2sphere: function(input, output, opts) {
      
      // console.log('********');
      // console.log('inputs: ');
      // console.log(input);
      // console.log('output: ');
      // console.log(output);
      // console.log('opts: ');
      // console.log(opts);
      // console.log('++++++++');

      var cube2sphereCache;

      function getPointOnUnitCube(x, y, z) {

        var tmin = -100, tmax = 100;
        var result = {x: 1, y: 0, z: 0};

        if(!areEqual(x, 0)) {

          var xInv = 1 / x;
          var x1 = -xInv;
          var x2 = xInv;
          tmin = Math.max(tmin, Math.min(x1, x2));
          tmax = Math.min(tmax, Math.max(x1, x2));

        }

        if(!areEqual(y, 0)) {

          var yInv = 1 / y;
          var y1 = -yInv;
          var y2 = yInv;;
          tmin = Math.max(tmin, Math.min(y1, y2));
          tmax = Math.min(tmax, Math.max(y1, y2));

        }

        if(!areEqual(z, 0)) {

          var zInv = 1 / z;
          var z1 = -zInv;
          var z2 = zInv;;
          tmin = Math.max(tmin, Math.min(z1, z2));
          tmax = Math.min(tmax, Math.max(z1, z2));

        }
        result.x = tmin * x;
        result.y = tmin * y;
        result.z = tmin * z;
        return result;

      };

      function getColor(x, y, source) {

        if(source.getPixel === undefined) {

          return {r: 255, g: 0, b: 0, a: 100};

        }

        var x = 0.5 * x + 0.5;
        var y = 0.5 * y + 0.5;
        var i = Math.floor(x * (source.width() - 1));
        var j = Math.floor(y * (source.height() - 1));
        return source.getPixel(i, j);

      };
      
      function getCubeColorAt(x, y, z) {

        var color = {r: 0, g: 255, b: 255, a: 100};
        if(areEqual(x, 1))
          return getColor(y, z, input.right);

        if(areEqual(x, -1))
          return getColor(y, z, input.left);

        if(areEqual(y, 1))
          return getColor(x, z, input.top);

        if(areEqual(y, -1))
          return getColor(x, z, input.bottom);

        if(areEqual(z, 1))
          return getColor(x, y, input.front);

        if(areEqual(z, -1))
          return getColor(x, y, input.back);

        return color;

      };

      function main() {

        var width = output.width();
        var height = output.height();
        var batch = output.batch();
        var denomX = 1 / (width - 1);
        var denomY = 1 / (height - 1);
        var aaSamples = opts.aaSamples;
        var sampleWidth = denomX / aaSamples;
        var sampleHeight = denomY / aaSamples;
        var sampleWeight = 1 / (aaSamples * aaSamples);

        for(var i = 0; i < width; i++) {

          for(var j = 0; j < height; j++) {

            // Normalized
            var nx = 2 * i * denomX - 1;
            var ny = 2 * j * denomY - 1;

            var avgColor = {r: 0, g: 0, b: 0, a: 100};
            for(var si = 1; si <= aaSamples; si++) {

              for(var sj = 1; sj <= aaSamples; sj++) {
                
                var u, v;
                if(opts.jittered) {

                  u = nx + si * sampleWidth * Math.random();
                  v = ny + sj * sampleHeight * Math.random();

                } else {

                  u = nx + si * sampleWidth * 0.5;
                  v = ny + sj * sampleHeight * 0.5;

                }

                // Spherical coords
                var theta = u * Math.PI;
                var phi = v * HALF_PI;

                var p;
                // var cached = cube2sphereCache !== undefined ? cube2sphereCache[theta] : undefined;
                // if(cube2sphereCache !== undefined) console.log('cached is not undefined!');
                // if(cached !== undefined && (cached = cached[phi]) !== undefined) {

                //   console.log('reading from cache!');
                //   p = cached;

                // } else {

                  // Cartesian coords
                  var x = Math.cos(theta) * Math.cos(phi);
                  var y = Math.sin(phi);
                  var z = Math.sin(theta) * Math.cos(phi);

                  // Coordinates on cube
                  p = getPointOnUnitCube(x, y, z);

                // }

                var color = getCubeColorAt(p.x, p.y, p.z);
                avgColor.r += color.r;
                avgColor.g += color.g;
                avgColor.b += color.b;
                // avgColor.a += color.a;

              }

            }

            avgColor.r = Math.floor(avgColor.r * sampleWeight);
            avgColor.g = Math.floor(avgColor.g * sampleWeight);
            avgColor.b = Math.floor(avgColor.b * sampleWeight);
            // avgColor.a = Math.floor(avgColor.a * this._sampleWeight);
            // console.log(avgColor);
            batch.setPixel(i, j, avgColor);

          }

        }

        return batch;

      };

      // function loadCube2sphereCache() {
      
      //   fs.readFile('.cube2sphereCache.json', function(error, data) {

      //     if(error) {

      //       console.log(error);
      //       return;

      //     };
      //     cube2sphereCache = JSON.parse(data);
      //     console.log('finished reading this shit!');

      //   });

      // };

      // loadCube2sphereCache();
      return(main());

    },

    sphere2cube: function() {

      throw {

        name: 'NotImplementedError',
        message: ''

      };

    },

    cube2cyl: function() {

      throw {

        name: 'NotImplementedError',
        message: ''

      };

    },

    cyl2cube: function() {

      throw {

        name: 'NotImplementedError',
        message: ''

      };

    },

    sphere2cyl: function() {

      throw {

        name: 'NotImplementedError',
        message: ''

      };

    },

    cyl2sphere: function() {

      throw {

        name: 'NotImplementedError',
        message: ''

      };

    }
  
  };

}();

module.exports = converters;