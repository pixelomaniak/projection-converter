﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections.Concurrent;

namespace Cube2Sphere
{
    class Program
    {
        private static double _equalityThreshold = 0.0000000001;
        private struct Vector3
        {
            public double x;
            public double y;
            public double z;
        }
        private static Bitmap[] _cubemap;
        private static Bitmap _output;
        private static object locker = new object();

        public static void Main(string[] args)
        {
            // Temporary parse input
            string[] files = new string[6];
            int width = 1024, height = 1024, numAASamples = 1;
            bool jittered = true;
            string output = "spheremap.png";
            try
            {
                files[0] = args[0];
                files[1] = args[1];
                files[2] = args[2];
                files[3] = args[3];
                files[4] = args[4];
                files[5] = args[5];
                width = int.Parse(args[6]);
                height = int.Parse(args[7]);
                numAASamples = int.Parse(args[8]);
                jittered = bool.Parse(args[9]);
                output = args[10];
            }
            catch(Exception e)
            {
                TerminateApplication(e, "Error while parsing the user input.");
            }

            try
            {
                InitCubeMap(files);
            }
            catch (AggregateException e)
            {
                TerminateApplication(e, "Error while loading cubemap.");
            }

            _output = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format64bppArgb);

            ParallelConvert(numAASamples, jittered, output);
        }

        private static void TerminateApplication(Exception e, string msg)
        {
            Console.WriteLine(e);
            Console.WriteLine(msg);
            Console.WriteLine("Terminating the application.");
            Console.ReadLine();
            Environment.Exit(-1);
        }

        private static void InitCubeMap(string[] files)
        {
            _cubemap = new Bitmap[6];
            var exceptions = new ConcurrentQueue<Exception>();
            Parallel.For(0, files.Length, index =>
            {
                string file = files[index];
                try
                {
                    _cubemap[index] = new Bitmap(file);
                }
                catch (Exception e)
                {
                    exceptions.Enqueue(e);
                }
            });
            if (exceptions.Count > 0)
            {
                throw new AggregateException(exceptions);
            }

            Console.WriteLine("Finished loading cubemap.");
        }

        private static void ParallelConvert(int numAASamples, bool jittered, string output)
        {
            int width = _output.Width;
            int height = _output.Height;
            double denomX = 1.0 / (width - 1);
            double denomY = 1.0 / (height - 1);
            double sampleWidth = denomX / numAASamples;
            double sampleHeight = denomY / numAASamples;
            int numAASamplesSquared = numAASamples * numAASamples;
            Random rand = new Random();
            var exceptions = new ConcurrentQueue<Exception>();
            Parallel.For(0, width, i =>
            {
                try
                {
                    for (int j = 0; j < height; j++)
                    {
                        double nx = 2.0 * i * denomX - 1.0;
                        double ny = 2.0 * j * denomY - 1.0;

                        int r = 0;
                        int g = 0;
                        int b = 0;
                        int a = 0;
                        for (int si = 1; si <= numAASamples; si++)
                        {
                            for (int sj = 1; sj <= numAASamples; sj++)
                            {
                                double sx;
                                double sy;
                                if (jittered)
                                {
                                    sx = nx + si * sampleWidth * rand.NextDouble();
                                    sy = ny + sj * sampleHeight * rand.NextDouble();
                                }
                                else
                                {
                                    sx = nx + si * sampleWidth * 0.5;
                                    sy = ny + sj * sampleHeight * 0.5;
                                }

                                // Spherical coordinates
                                double theta = sx * Math.PI;
                                double phi = sy * Math.PI * 0.5;

                                // Cartesian coordinates
                                Vector3 cartesian;
                                cartesian.x = Math.Cos(phi) * Math.Cos(theta);
                                cartesian.y = Math.Sin(phi);
                                cartesian.z = Math.Cos(phi) * Math.Sin(theta);

                                // TODO: Add rotation

                                // TODO: Optimise this to avoid costly intersections
                                Vector3 intersectionPoint = IntersectWithCube(cartesian);

                                Color color = GetCubeColor(intersectionPoint);
                                r += color.R;
                                g += color.G;
                                b += color.B;
                                a += color.A;
                            }
                        }
                        r /= numAASamplesSquared;
                        g /= numAASamplesSquared;
                        b /= numAASamplesSquared;
                        a /= numAASamplesSquared;
                        //r = Math.Max(0, Math.Min(r, 255));
                        //g = Math.Max(0, Math.Min(g, 255));
                        //b = Math.Max(0, Math.Min(b, 255));
                        //a = Math.Max(0, Math.Min(a, 255));

                        Color averageColor = Color.FromArgb(a, r, g, b);
                        lock (locker)
                        {
                            //Console.WriteLine(averageColor);
                            _output.SetPixel(i, j, averageColor);
                        }
                    }
                }
                catch(Exception e)
                {
                    exceptions.Enqueue(e);
                }
            });

            if(exceptions.Count > 0)
            {
                var e = new AggregateException(exceptions);
                TerminateApplication(e, "Error while converting.");
            }
            // TODO: User defined format
            _output.Save(output, System.Drawing.Imaging.ImageFormat.Jpeg);
            Console.WriteLine("Finished conversion in {0} seconds.");
        }

        private static Vector3 IntersectWithCube(Vector3 p)
        {
            Vector3 result;
            double tmin = -100.0, tmax = 100.0;

            if (Math.Abs(p.x) > _equalityThreshold)
            {
                double xInv = 1.0 / p.x;
                double x1 = -xInv;
                double x2 = xInv;
                tmin = Math.Max(tmin, Math.Min(x1, x2));
                tmax = Math.Min(tmax, Math.Max(x1, x2));
            }

            if (Math.Abs(p.y) > _equalityThreshold)
            {
                double yInv = 1.0 / p.y;
                double y1 = -yInv;
                double y2 = yInv; ;
                tmin = Math.Max(tmin, Math.Min(y1, y2));
                tmax = Math.Min(tmax, Math.Max(y1, y2));
            }

            if (Math.Abs(p.z) > _equalityThreshold)
            {
                var zInv = 1.0 / p.z;
                var z1 = -zInv;
                var z2 = zInv; ;
                tmin = Math.Max(tmin, Math.Min(z1, z2));
                tmax = Math.Min(tmax, Math.Max(z1, z2));
            }

            result.x = p.x * tmin;
            result.y = p.y * tmin;
            result.z = p.z * tmin;
            return result;
        }

        private static Color GetCubeColor(Vector3 p)
        {
            lock (locker)
            {
                int i, j, width, height;
                if (Math.Abs(p.x - 1.0) < _equalityThreshold)
                {
                    width = _cubemap[1].Width;
                    height = _cubemap[1].Height;
                    GetCoordinates(p.z, -p.y, width, height, out i, out j);
                    return _cubemap[1].GetPixel(i, j);
                }
                if (Math.Abs(p.x + 1.0) < _equalityThreshold)
                {
                    width = _cubemap[0].Width;
                    height = _cubemap[0].Height;
                    GetCoordinates(-p.z, -p.y, width, height, out i, out j);
                    return _cubemap[0].GetPixel(i, j);
                }
                if (Math.Abs(p.y - 1.0) < _equalityThreshold)
                {
                    width = _cubemap[4].Width;
                    height = _cubemap[4].Height;
                    GetCoordinates(-p.z, -p.x, width, height, out i, out j);
                    return _cubemap[4].GetPixel(i, j);
                }
                if (Math.Abs(p.y + 1.0) < _equalityThreshold)
                {
                    width = _cubemap[5].Width;
                    height = _cubemap[5].Height;
                    GetCoordinates(-p.z, p.x, width, height, out i, out j);
                    return _cubemap[5].GetPixel(i, j);
                }
                if (Math.Abs(p.z - 1.0) < _equalityThreshold)
                {
                    width = _cubemap[3].Width;
                    height = _cubemap[3].Height;
                    GetCoordinates(-p.x, -p.y, width, height, out i, out j);
                    return _cubemap[3].GetPixel(i, j);
                }
                if (Math.Abs(p.z + 1.0) < _equalityThreshold)
                {
                    width = _cubemap[2].Width;
                    height = _cubemap[2].Height;
                    GetCoordinates(p.x, -p.y, width, height, out i, out j);
                    return _cubemap[2].GetPixel(i, j);


                }
                return Color.Red;
            }
        }

        private static void GetCoordinates(double x, double y, int width, int height, out int i, out int j)
        {
            x = 0.5 * x + 0.5;
            y = 0.5 * y + 0.5;
            i = (int)Math.Floor(x * (width - 1));
            j = (int)Math.Floor(y * (height - 1));
        }
    }
}